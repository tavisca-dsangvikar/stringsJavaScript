function showButton(){
	var choice=document.getElementById("dropdown").value;
	

	if (choice == "Length" || choice == "Concat" || choice == "CharAt" || choice == "IndexOf") 
	{
			document.getElementById("text1").style.visibility = 'visible';
			document.getElementById("text2").style.visibility = 'visible';
		   document.getElementById("text3").style.visibility = 'hidden';
  		   document.getElementById("text4").style.visibility = 'hidden';
  		   document.getElementById("text3Label").style.visibility='hidden';
  		   document.getElementById("text4Label").style.visibility='hidden';
  		   
  		   
	};
	if (choice == "Substring") 
	{
		   	document.getElementById("text1").style.visibility = 'visible';
			document.getElementById("text2").style.visibility = 'hidden';
		   document.getElementById("text3").style.visibility = 'visible';
  		   document.getElementById("text4").style.visibility = 'visible';
  		   document.getElementById("text3Label").style.visibility='visible';
  		   document.getElementById("text3Label").innerHTML="Starting Index";
 		   document.getElementById("text4Label").style.visibility='visible';
  		   document.getElementById("text4Label").innerHTML="End Index";
  		   
	};
	
	if (choice == "Replace") 
	{
		document.getElementById("text1").style.visibility = 'visible';
			document.getElementById("text2").style.visibility = 'hidden';
		   document.getElementById("text3").style.visibility = 'visible';
  		   document.getElementById("text4").style.visibility = 'visible';
  		   document.getElementById("text3Label").style.visibility='visible';
  		   document.getElementById("text3Label").innerHTML="Substring to be replaced";
 		   document.getElementById("text4Label").style.visibility='visible';
  		   document.getElementById("text4Label").innerHTML="Replacement string";
  		   document.getElementById("string2").style.visibility='hidden';
  		   
	};
	if (choice == "LastIndexOf") 
	{
		document.getElementById("text1").style.visibility = 'visible';
			document.getElementById("text2").style.visibility = 'visible';
		   document.getElementById("text3").style.visibility = 'hidden';
  		   document.getElementById("text4").style.visibility = 'hidden';
  		   document.getElementById("text3Label").style.visibility='hidden';
  		document.getElementById("text4Label").style.visibility='hidden';
  		document.getElementById("string2").innerHTML="Element to be found";
  		   
  		   	   
	};
	
}




var stringExp = function(val){
    this.value = val;

    this.myLen = function(){
    var i =0;
    debugger;
    while(this.value[i]!=null)
        i+= 1;
    return i;
	}

    this.concat = function(str){
    	var ret = this.value + str.value;
    	return ret; 
    }

    this.mySubString = function(i,j){
    	var ch="";
    	for(var k=i;k<j;k++)
    		ch+=this.value[k];
    	debugger;
    	return ch; 
    }

    this.myCharAt = function(i) {
    	var ch = this.value[i];
    	return ch;
    }

    this.myIndex= function(i){
    	return this.value.indexOf(i);
    }

    this.myLast = function(j){
    	debugger;
    	for (var i = (this.value.length - 1); i >= 0; i--) {
    		if (this.value[i] == j) {
    			return i;
    		};
    	};
    	return -1;
    }

    this.myReplace = function(str,rep){
    	var i= this.value.indexOf(str);
    	var j = 0;
    	var res  = this.value;
    	for (var k = i; i < str.Length; k++) {
    		this.value[k] = rep[j];
    		j++;
    	};
    }
    
}


function myFunction(){

	var choice=document.getElementById("dropdown").selectedIndex;

	var str1 = new stringExp(document.getElementById('text1').value);
	var str2 = new stringExp(document.getElementById('text2').value);

	switch(choice)
	{
		case 0:var s=str1.myLen();
				document.getElementById("answer").innerHTML=s;
				return;

		case 1:var s=str1.concat(str2);
				document.getElementById("answer").innerHTML=s;
				return;

		case 2:  	var s= str1.mySubString(document.getElementById("text3").value,document.getElementById("text4").value)
				document.getElementById("answer").innerHTML=s;

					return;

		case 3:    
					var ch= str1.myCharAt(document.getElementById("text2").value);
				document.getElementById("answer").innerHTML=ch;
					return;


		case 4: var val=str1.myIndex(document.getElementById("text2").value);
				document.getElementById("answer").innerHTML=val;
				return;

		case 5: 
				var val=str1.myLast(document.getElementById("text2").value);
				if(val != -1)
					document.getElementById("answer").innerHTML=val;
				else
					document.getElementById("answer").innerHTML="Element not found";
				return;

		case 6: 
				var v=str1.myReplace(document.getElementById("text3").value,document.getElementById("text4").value);
				document.getElementById("answer").innerHTML=v;
				return;

			
	}
	
}



function hide( ){
 document.getElementById("text3").style.visibility = 'hidden';
 document.getElementById("text4").style.visibility = 'hidden';
}

var click = document.getElementById("buttonCompute");
click.onclick = myFunction;